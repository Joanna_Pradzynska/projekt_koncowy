$(document).ready(function(){

	$('.navbar-nav').on('click', 'li', function() {
		$(this).parent().children('li').removeClass('active');
		$(this).addClass('active');
	});

  	// Add smooth scrolling to all links in navbar + footer link
  	$(".navbar a, footer a[href='#home']").on('click', function(event) {
	    // Make sure this.hash has a value before overriding default behavior
	    if (this.hash !== "") {
	      // Prevent default anchor click behavior
	      event.preventDefault();

	      // Store hash
	      var hash = this.hash;

	      // Using jQuery's animate() method to add smooth page scroll
	      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
	      $('html, body').animate({
	      	scrollTop: $(hash).offset().top
	      }, 900, function(){

	        // Add hash (#) to URL when done scrolling (default click behavior)
	        window.location.hash = hash;
	    	});
	    } // End if
	});

// regulamin

	$('#regulamin').on('click', 'input', function() {
		var btnText = $(this).val();
		if (btnText == 'Pokaż regulamin') {
			$(this).val('Ukryj regulamin');
		}
		else {
			$(this).val('Pokaż regulamin');
		}
		$(this).parent().find('.regulamin').fadeToggle();
	});


  $("#owl-demo").owlCarousel({
 
      autoPlay: 3000, //Set AutoPlay to 3 seconds
      items : 3,
      itemsDesktop : [1199,3],
      itemsDesktopSmall : [979,3],
  });


});
