var searchCarBox = function(){
	var bd = [
				{'brand' : 'Ford', 'type': 'Focus Station', 'photo' : 'ford_focus_station.png', 'pricePerDay': 120, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Ford', 'type': 'Galaxy TDCI', 'photo' : 'ford_galaxy_TDCI.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 7, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Ford', 'type': 'Tourneo Connect', 'photo' : 'ford_tourneo_connect.png', 'pricePerDay': 125, 'doors' : 5, 'people' : 7, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Hyundai', 'type': 'i10', 'photo' : 'hyundai_i10.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 4, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Mercedes', 'type': 'C-Class', 'photo' : 'mercedes_C-Class.png', 'pricePerDay': 180, 'doors' : 4, 'people' : 5, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Nissan', 'type': 'Qashqai', 'photo' : 'nissan_qashqai.png', 'pricePerDay': 100, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Opel', 'type': 'Astra', 'photo' : 'opel_astra.png', 'pricePerDay': 105, 'doors' : 4, 'people' : 5, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Renault', 'type': 'Traffic', 'photo' : 'ranault_trafic.png', 'pricePerDay': 120, 'doors' : 4, 'people' : 9, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Renault', 'type': 'Clio', 'photo' : 'renault_clio.png', 'pricePerDay': 90, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Renault', 'type': 'Megane', 'photo' : 'renault_megane.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Skoda', 'type': 'Octavia', 'photo' : 'skoda_octavia.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Skoda', 'type': 'Octavia Stat ???', 'photo' : 'skoda_octavia_stat.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Volvo', 'type': 'XC60', 'photo' : 'volvo_XC60.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'VW', 'type': 'Passat', 'photo' : 'vw_passat.png', 'pricePerDay': 130, 'doors' : 4, 'people' : 5, 'AC' : true, 'gearbox' : 'manualna'},
				{'brand' : 'Passat', 'type': 'Passat ???', 'photo' : 'vw_passat2.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'manualna'}
	];


	var ulTag = $('#oferta .cars');
	var searchText = $('#searchCar');
	ulTag.empty();

	bd.forEach(function(car){	
		var liTag = $('<li></li>');
		var carName = $('<h3>' + car["brand"] + ' ' + car["type"] + '</h3>');
		var deal = $('<p><span class="deal">Super oferta!</span></p>');
		var carPhoto = $('<img src="img/cars/' + car["photo"] + '"/>');
		var pricePerDay = $('<p>Cena za dobę: <span class="dayPrice">' + car["pricePerDay"] + '</span>zł</p>');
		var numbOfDays = $('<p><input type="number" class="days" value="1" min="0" max="365"></p>');
		var totalPrice = $('<p>Całkowita kwota: <span class="total">' + car["pricePerDay"] + '</span> zł</p>');
		var btnInfo = $('<button class="btnInfo"><span class="glyphicon glyphicon-info"></span>Szczegóły</button>');

		var divTag = $('<div class="moreInfo"></div>');

		var features = $('<table><tr><td>drzwi</td><td>' + car["doors"] + '</td></tr><tr><td>pasażerowie</td><td>' + car["people"] + '</td></tr><tr><td>klimatyzacja</td><td>' + car["AC"] + '</td></tr><tr><td>skrzynia biegów</td><td>' + car["gearbox"] + '</td></tr></table> ');

		$(divTag).append(features);

		$(liTag).append(carName).append(deal).append(carPhoto).append(pricePerDay).append(numbOfDays).append(totalPrice).append(btnInfo).append(divTag);

		if(searchText.val() != '') {
			var matcher = new RegExp(searchText.val(), "gi");
			if(matcher.test(car["brand"]) || matcher.test(car["type"])) {
				$(ulTag).append(liTag);
			}
		} else {
			$(ulTag).append(liTag);
		}
	});

	$('#oferta .cars').on('keyup mouseup', '.days', function() {
		var dayPrice = +$(this).closest('li').find('.dayPrice').html();
		var dayNumb = $(this).val();
		$(this).closest('li').find('.total').html(dayNumb * dayPrice);
	});

	$('#oferta .cars').on('mouseenter mouseleave', 'li', function(){
		$(this).find('.moreInfo').fadeToggle();
	});

	$('#oferta .cars').on('mouseenter', 'li', function(){
		$(this).find('.deal').animate({'top' : '-14px', 'opacity': '1',}, 'fast');		
	});

	$('#oferta .cars').on('mouseleave', 'li', function(){
		$(this).find('.deal').animate({'top' : '0px', 'opacity': '0',}, 'fast');		
	});
}

$(document).ready(function() {
	searchCarBox();
	$('#searchCar').on('keyup', function() {
		searchCarBox();
	});
});
